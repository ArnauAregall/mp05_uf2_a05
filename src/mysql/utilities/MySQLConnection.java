package mysql.utilities;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;

public class MySQLConnection {
    
    private String DRIVER = "com.mysql.jdbc.Driver";
    private String SERVER = "jdbc:mysql://localhost/";
    private String db;
    private String usr;
    private String pwd;
    private Connection conn = null;
    
    
    public MySQLConnection() {
        db = MySQLConstraints.DB;
        usr = MySQLConstraints.UNAME;
        pwd = MySQLConstraints.PWD;
    }
    
    public Connection getConnection() {
        return this.conn;
    }
    
    public final void connect() {
        if (conn == null) {
            try {
                Class.forName(DRIVER);
                conn = (Connection) DriverManager.getConnection(SERVER+db,usr,pwd);
                System.out.println("Connected to DB "+db+" successfully.");
            } catch (Exception e) {
                System.out.println("Exception Connecting to Database : "+e);
             }
        }
    }
    
    public final void disconnect() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
                System.out.println("Disconnected from DB "+db+" successfully.");
            } catch (Exception e) {
                System.out.println("Exception Disconnecting from Database : "+e);
            }
        }
    }
    
    
}