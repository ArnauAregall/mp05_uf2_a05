package mysql.utilities;

import com.mysql.jdbc.ResultSetMetaData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

/**
 * UtilitiesHelper
 * Class that provides static methods that are commonly used when working with DB.
 * @author ArnauAregall
 *
 */
public class UtilitiesHelper {
    	
    public static boolean isBiggerThanZero(String value) {
        boolean isBigger = (Integer.parseInt(value) > 0) ? true : false;
        return isBigger;
    }
    
    public static boolean isBiggerOrEqualsZero(String value) {
        boolean condition = (Integer.parseInt(value) >= 0 ) ? true : false;
        return condition;
    }
    
    /**
     * returns boolean if exists parametrized field-value in parametrized table
     * @param con, Connection
     * @param table, table name
     * @param field, field name 
     * @param value, 
     * @return boolean
     * @throws SQLException
     */
    public static boolean checkIfExists(Connection con, String table, String field, String value) throws SQLException {
        boolean exists = true;
        Statement stmnt = null;
        ResultSet rs = null;
        try {
            stmnt = (Statement) con.createStatement();
            rs = stmnt.executeQuery("SELECT COUNT(*) FROM "+table+" WHERE "+field+" = "+value);
            rs.next();
            int count = rs.getInt(1);
            exists = (count > 0) ? true : false;
        } catch (SQLException e) {
            System.out.println("Exception @ checkIfExists : " + e);
        } finally {
           if(rs != null) { rs.close(); }
           if(stmnt != null) { stmnt.close(); }
        }
        return exists;
    }
    
    public static boolean isNumeric(String str) {  
        try  {  
          @SuppressWarnings("unused")
		double d = Double.parseDouble(str);  
        } catch(NumberFormatException nfe)  {  
          return false;  
        }  
        return true;  
    }
    
    /**
     * Returns string array with the column names of the table parametrized
     * @param con, Connection
     * @param tableName
     * @return String[] 
     * @throws SQLException
     */
    public static String[] getTableColumnNames(Connection con ,String tableName) throws SQLException {
    	String[] columns = null;
    	Statement stmnt = null;
    	ResultSet rs = null;
    	ResultSetMetaData rsmd = null;
    	if(con != null) {
    		try {
    			String query = "SELECT * FROM " +MySQLConstraints.DB+"."+tableName;
    			stmnt = con.createStatement();
    			rs = stmnt.executeQuery(query);
    			rsmd = (ResultSetMetaData) rs.getMetaData();
    			int cols = rsmd.getColumnCount();
    			columns = new String[cols];
    			for (int y = 0; y < columns.length; y++) {
    				columns[y] = rsmd.getColumnName(y+1);
    			}
    		} catch(Exception ex) {
    			System.out.println("Exception @ UtilitiesHelper.getTableColumnNames : " + ex);
    		} finally {
    			if(rs != null) { rs.close(); }
    			if(stmnt != null) {stmnt.close(); }
    		}
    	}
    	return columns;
    }
    
}