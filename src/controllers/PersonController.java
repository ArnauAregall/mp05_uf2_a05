package controllers;


import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.Context;
import javax.swing.JOptionPane;

import models.Person;
import mysql.utilities.MySQLConstraints;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class PersonController {
	
	/**
	 * Retrieves an ArrayList of Persons object from "person" table in DB.
	 * @param con (Connection)
	 * @return ArrayList<Person>
	 * @throws SQLException
	 * @author aaregall
	 */
	public ArrayList<Person> getPersons(Connection con) throws SQLException {
		ArrayList<Person> persons = new ArrayList<Person>();
		Statement stmnt = null;
		ResultSet rs = null;
		try {
			String query = "SELECT id AS ID, " +
						   "name AS NAME, " +
						   "surname AS SURNAME, " +
						   "age AS AGE, " +
						   "gender AS GENDER, " +
						   "email AS EMAIL, " +
						   "address AS ADDRESS, " +
						   "city AS CITY, " +
						   "postalcode AS POSTALCODE, " +
						   "phone AS PHONE, " +
						   "creationdate AS CREATIONDATE " +
						   "FROM " + MySQLConstraints.DB+".person ;";
			stmnt = (Statement) con.createStatement();
			rs = stmnt.executeQuery(query);
			while (rs.next()) {
				Person person = new Person(rs.getInt("ID"), rs.getString("NAME"), rs.getString("SURNAME"), rs.getInt("AGE"), 
						rs.getString("GENDER"), rs.getString("EMAIL"), rs.getString("ADDRESS"), rs.getString("CITY"),
						rs.getString("POSTALCODE"), rs.getString("PHONE"), rs.getString("CREATIONDATE"));
				persons.add(person);
			}
		} catch (Exception e) {
			System.out.println("Exception @ PersonController.getPersons() : " +e);
		} finally {
			if (rs != null) {rs.close();}
			if (stmnt != null) {stmnt.close();}
		}
		return persons;
	}
	
	/**
	 * Method to insert a register into "person" DB table. Shows success dialog if everything is correct.
	 * @param con (Connection)
	 * @param person 
	 * @param context
	 * @throws SQLException
	 * @author aaregall
	 */
	
	public void insertPerson(Connection con, Person person, Context context) throws SQLException {
		Statement stmnt = null;
		try {
			stmnt = (Statement) con.createStatement();
			String insert = "INSERT INTO person (name, surname, age, gender, email, address, city, postalcode, phone, creationdate) VALUES (" +
					"'" +person.getName() + "' , " +
					"'" +person.getSurname() + "' , " +
					person.getAge() + ", " +
					"'" +person.getGender() + "' , " +
					"'" +person.getEmail() + "' , " +
					"'" +person.getAddress() + "' , " +
					"'" +person.getCity() + "' , " +
					"'" +person.getPostalCode() + "' , " +
					"'" +person.getPhone() + "' , " +
					"'" +person.getCreationDate() + "' " +
					")";
			stmnt.executeUpdate(insert);
			JOptionPane.showMessageDialog((Component) context, "Persona insertada correctamente","Persona insertada", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Exception @ PersonController.insertPerson : " + e);
		} finally {
			if (stmnt != null) { stmnt.close(); }
		}
	}
	
	/**
	 * Method to update a person register
	 * @param con
	 * @param person
	 */
	public void updatePerson(Connection con, Person person, Context context) throws SQLException {
		Statement stmnt = null;
		try {
			stmnt = (Statement) con.createStatement();
			String update = "UPDATE person SET " +
					"name = '"+person.getName()+"', " +
					"surname = '"+person.getSurname()+"' , " +
					"age = "+person.getAge()+", " +
					"gender =  '"+person.getGender()+"', " +
					"email =  '"+person.getEmail()+"', " +
					"address = '"+person.getAddress()+"', " +
					"city = '"+person.getCity()+"', " +
					"postalcode =  '"+person.getPostalCode()+"', " +
					"phone = '"+person.getPhone()+"' " +
					"WHERE id = " + person.getId();
			stmnt.executeUpdate(update);
			JOptionPane.showMessageDialog((Component) context, "Persona modificada correctamente","Persona insertada", JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Exception @ PersonController.insertPerson : " + e);
		} finally {
			if (stmnt != null) { stmnt.close(); }
		}
	}
	
}
