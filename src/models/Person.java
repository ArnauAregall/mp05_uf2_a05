package models;

/**
 * Person class, used to create model objects of "person" table form database.
 * @author ArnauAregall
 * 
 */
public class Person {
	
	private int _id;
	private String _name;
	private String _surname;
	private int _age;
	private String _gender;
	private String _email;
	private String _address;
	private String _city;
	private String _postalCode;
	private String _phone;
	private String _creationDate;
	
	/**
	 * Person constructor
	 * @param id
	 * @param name
	 * @param surname
	 * @param age
	 * @param gender
	 * @param email
	 * @param address
	 * @param city
	 * @param postalCode
	 * @param phone
	 * @param creationDate
	 */
	public Person(int id, String name, String surname, int age, String gender, 
				  String email, String address, String city, String postalCode,
				  String phone, String creationDate) {
		this._id = id;
		this._name = name;
		this._surname = surname;
		this._age = age;
		this._gender = gender;
		this._email = email;
		this._address = address;
		this._city = city;
		this._postalCode = postalCode;
		this._phone = phone;
		this._creationDate = creationDate;
	}
	
	// Getters and Setters for private properties
	public int getId() {
		return _id;
	}
	public void setId(int id) {
		this._id = id;
	}
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}
	public String getSurname() {
		return _surname;
	}
	public void setSurname(String surname) {
		this._surname = surname;
	}
	public int getAge() {
		return _age;
	}
	public void setAge(int age) {
		this._age = age;
	}
	public String getGender() {
		return _gender;
	}
	public void set_gender(String gender) {
		this._gender = gender;
	}
	public String getEmail() {
		return _email;
	}
	public void setEmail(String email) {
		this._email = email;
	}
	public String getAddress() {
		return _address;
	}
	public void setAddress(String address) {
		this._address = address;
	}
	public String getCity() {
		return _city;
	}
	public void setCity(String city) {
		this._city = city;
	}
	public String getPostalCode() {
		return _postalCode;
	}
	public void setPostalCode(String postalCode) {
		this._postalCode = postalCode;
	}
	public String getPhone() {
		return _phone;
	}
	public void setPhone(String phone) {
		this._phone = phone;
	}
	public String getCreationDate() {
		return _creationDate;
	}
	public void setCreationDate(String creationDate) {
		this._creationDate = creationDate;
	}
}
