package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controllers.PersonController;

import models.Person;
import mysql.utilities.MySQLConnection;

public class PersonView extends JFrame implements ActionListener{

	// MVC variables
	private static MySQLConnection conn = null;
	private static PersonView view = null;
	private static PersonController controller = null;
	
	private static ArrayList<Person> persons = null;
	private static int personIndex = 0;
	/**
	 * main method
	 * @param args
	 */
	public static void main(String args[] ){
		controller = new PersonController();
		conn = new MySQLConnection();
		try{
			conn.connect();
			persons = controller.getPersons(conn.getConnection());
		} catch (Exception ex) {
			System.out.println("Exception @ main : " + ex);
		}
		
		view = new PersonView(false);
		view.showUp();
		// closing connection when view is closed
        windowAdapter = new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                conn.disconnect();
                System.exit(0); 
            }
        };
        view.addWindowListener(windowAdapter);
	}
	
	
	// Person View 
	// ************************************************
	// GUI
	private JPanel _pnlPrincipal;
	private JLabel 	_lblId, _lblName,_lblSurname, _lblAge, _lblGender ,_lblEmail , _lblAddress,
					_lblCity, _lblPostalCode, _lblPhone, _lblCreationDate;
	
	private JTextField 	_tfId, _tfName, _tfSurname, _tfAge, _tfGender,_tfAddress, _tfEmail, _tfCity,
						_tfPostalCode, _tfPhone ,_tfCreationDate;
	private JButton _btnEdit, _btnNext , _btnPrevious, _btnCreate, _btnSave, _btnCancel, _btnViewAll;
	private ArrayList<JTextField> _allTfs;
	private boolean _isEditable; // needed on constructor
	private static WindowAdapter windowAdapter; // used to implement own "on close" frame method
	
	/**
	 * Constructor
	 * @param isEditable
	 */
	public PersonView(boolean isEditable) {
		this._isEditable = isEditable;
		_pnlPrincipal = new JPanel(null);
		/**
		 * Labels
		 */
		_lblId 	 	= new JLabel("Person ID : ");
		_lblName 	= new JLabel("Name : ");
		_lblSurname = new JLabel("Surname : ");
		_lblAge 	= new JLabel("Age : ");
		_lblGender	= new JLabel("Gender : ");
		_lblEmail	= new JLabel("Email : ");
		_lblAddress	= new JLabel("Address : ");
		_lblCity	= new JLabel("City : ");
		_lblPostalCode = new JLabel("Postal Code : ");
		_lblPhone = new JLabel("Phone Number : ");
		_lblCreationDate = new JLabel("Creation Date : ");
		
		/**
		 * TextFields, create and add them into an ArrayList
		 */
		_allTfs = new ArrayList<JTextField>();
		
		_tfId		= new JTextField();
			_allTfs.add(_tfId);
		_tfName		= new JTextField();
			_allTfs.add(_tfName);
		_tfSurname 	= new JTextField();
			_allTfs.add(_tfSurname);
		_tfAge		= new JTextField();
			_allTfs.add(_tfAge);
		_tfGender 	= new JTextField();
			_allTfs.add(_tfGender);
		_tfEmail	= new JTextField();
			_allTfs.add(_tfEmail);
		_tfAddress	= new JTextField();
			_allTfs.add(_tfAddress);
		_tfCity		= new JTextField();
			_allTfs.add(_tfCity);
		_tfPostalCode = new JTextField();
			_allTfs.add(_tfPostalCode);
		_tfPhone	= new JTextField();
			_allTfs.add(_tfPhone);
		_tfCreationDate = new JTextField();
			_allTfs.add(_tfCreationDate);
		
		/**
		 * Buttons 	
		 */
		_btnEdit = new JButton("Edit");
		_btnNext = new JButton("Next");
		_btnPrevious = new JButton("Previous");
		_btnCreate = new JButton("Create");
		_btnSave = new JButton("Save");
		_btnCancel = new JButton("Cancel");
		_btnViewAll = new JButton("View All Persons");
		
		textFieldStatus(_isEditable);	
			
		/**
		 * Labels @ Positions
		 */
		_lblId.setBounds(10, 10, 150, 25);
		_pnlPrincipal.add(_lblId);
		_lblName.setBounds(10, 35, 150, 25);
		_pnlPrincipal.add(_lblName);
		_lblSurname.setBounds(10, 60, 150, 25);
		_pnlPrincipal.add(_lblSurname);
		_lblAge.setBounds(10,85, 150, 25);
		_pnlPrincipal.add(_lblAge);
		_lblGender.setBounds(10,110, 150, 25);
		_pnlPrincipal.add(_lblGender);
		_lblEmail.setBounds(10, 135, 150, 25);
		_pnlPrincipal.add(_lblEmail);
		_lblAddress.setBounds(10, 160, 150, 25);
		_pnlPrincipal.add(_lblAddress);
		_lblCity.setBounds(10, 185, 150, 25);
		_pnlPrincipal.add(_lblCity);
		_lblPostalCode.setBounds(10, 210, 150, 25);
		_pnlPrincipal.add(_lblPostalCode);
		_lblPhone.setBounds(10, 235, 150, 25);
		_pnlPrincipal.add(_lblPhone);
		_lblCreationDate.setBounds(10, 260, 150, 25);
		_pnlPrincipal.add(_lblCreationDate);
		/**
		 * TextFields @ Position
		 */
		_tfId.setBounds(160, 10, 150, 25);
		_pnlPrincipal.add(_tfId);
		_tfName.setBounds(160,35,150,25);
		_pnlPrincipal.add(_tfName);
		_tfSurname.setBounds(160,60,150,25);
		_pnlPrincipal.add(_tfSurname);
		_tfAge.setBounds(160, 85, 150, 25);
		_pnlPrincipal.add(_tfAge);
		_tfGender.setBounds(160, 110, 150, 25);
		_pnlPrincipal.add(_tfGender);
		_tfEmail.setBounds(160, 135, 150, 25);
		_pnlPrincipal.add(_tfEmail);
		_tfAddress.setBounds(160, 160, 150, 25);
		_pnlPrincipal.add(_tfAddress);
		_tfCity.setBounds(160, 185, 150, 25);
			_pnlPrincipal.add(_tfCity);
		_tfPostalCode.setBounds(160, 210, 150, 25);
			_pnlPrincipal.add(_tfPostalCode);
		_tfPhone.setBounds(160, 235, 150, 25);
			_pnlPrincipal.add(_tfPhone);
		_tfCreationDate.setBounds(160, 260,150, 25);
			_pnlPrincipal.add(_tfCreationDate);
			
		_btnCreate.setBounds(10,310,300,25);
			_btnCreate.addActionListener(this);
			_pnlPrincipal.add(_btnCreate);
		_btnSave.setBounds(160,310,150,25);
			_btnSave.addActionListener(this);
			_btnSave.setVisible(false);
			_pnlPrincipal.add(_btnSave);
		_btnCancel.setBounds(10,310,150,25);
			_btnCancel.addActionListener(this);
			_btnCancel.setVisible(false);
			_pnlPrincipal.add(_btnCancel);
		
		_btnEdit.setBounds(10,340,300,25);
			_btnEdit.addActionListener(this);
			_pnlPrincipal.add(_btnEdit);
		_btnNext.setBounds(160,370,150,25);
			_btnNext.addActionListener(this);
			_pnlPrincipal.add(_btnNext);
		_btnPrevious.setBounds(10,370,150,25);
			_btnPrevious.addActionListener(this);
			_pnlPrincipal.add(_btnPrevious);
		_btnViewAll.setBounds(10,400,300,25);
			_btnViewAll.addActionListener(this);
			_pnlPrincipal.add(_btnViewAll);
			
		this.add(_pnlPrincipal);
		this.setTitle("MP05_UF2_A05");
		this.setSize(330,470);
		this.setResizable(false);
	}
	
	private void loadPersonInfo(int index) {
		Person person = persons.get(index);
		this._tfId.setText(Integer.toString(person.getId()));
		this._tfName.setText(person.getName());
		this._tfSurname.setText(person.getSurname());
		this._tfAge.setText(Integer.toString(person.getAge()));
		this._tfGender.setText(person.getGender());
		this._tfEmail.setText(person.getEmail());
		this._tfAddress.setText(person.getAddress());
		this._tfCity.setText(person.getCity());
		this._tfPostalCode.setText(person.getPostalCode());
		this._tfPhone.setText(person.getPhone());
		this._tfCreationDate.setText(person.getCreationDate());
		
		if(index <= 0) {
			this._btnPrevious.setEnabled(false);
		} else {
			this._btnPrevious.setEnabled(true);
		}
		
		// controlling if index points to last person
		if(index >= persons.size() - 1) {
			this._btnNext.setEnabled(false);
		} else {
			this._btnNext.setEnabled(true);
		}
	}
	
	/**
	 * Updates text field editable status
	 * @param status boolean, true editable, false no-editable
	 */
	
	private void textFieldStatus(boolean status) {
		for(JTextField tf : _allTfs){
			_tfId.setEditable(false);
			if(tf.equals(_tfName) || tf.equals(_tfSurname) || tf.equals(_tfAge)) {
				tf.setEditable(status);
			} else {
				tf.setEditable(false);
			}
		}
	}

	private void cleanTextFields() {
		for(JTextField tf : _allTfs){
				if(tf.equals(_tfId)){
					tf.setEditable(false);
				}else{
					tf.setEditable(true);
				}
				tf.setText("");
			}
	}
	
	/**
	 * Method to show the frame, used to load before showing it up the db data
	 */
    public void showUp() {
    	this.setVisible(true);
    	loadPersonInfo(personIndex);
    }
    
	// action Performed listener
	@Override
	public void actionPerformed(ActionEvent ae) {
		// button 
		if(ae.getSource().equals(_btnEdit)){
			if(_isEditable){
				_btnEdit.setText("Edit");
				textFieldStatus(false);
				_isEditable = false;
			}else{
				_btnEdit.setText("Save");
				textFieldStatus(true);
				_isEditable = true;
			}
		}
		if(ae.getSource().equals(_btnCreate)){
			_btnCreate.setVisible(false);
			_btnSave.setVisible(true);
			_btnCancel.setVisible(true);
			_btnEdit.setVisible(false);
			_btnPrevious.setVisible(false);
			_btnNext.setVisible(false);
			_btnViewAll.setVisible(false);
			_isEditable = true;
			this.setSize(330,400);
			cleanTextFields();
		}
		// next button
		if(ae.getSource().equals(_btnNext)){
			personIndex++;
			loadPersonInfo(personIndex);
		}
		// previous button
		if(ae.getSource().equals(_btnPrevious)){
			personIndex--;
			loadPersonInfo(personIndex);
		}
		// View All Persons
		if(ae.getSource().equals(_btnViewAll)){
			//TODO implements methods
		}
		// Cancel button
		if(ae.getSource().equals(_btnCancel)){
			_btnCreate.setVisible(true);
			_btnCancel.setVisible(false);
			_btnSave.setVisible(false);
			_btnEdit.setVisible(true);
			_btnNext.setVisible(true);
			_btnPrevious.setVisible(true);
			_btnViewAll.setVisible(true);
			personIndex=0;
			loadPersonInfo(personIndex);
			_isEditable = false;
			textFieldStatus(_isEditable);
			
			this.setSize(330,500);
		}
	}
}
