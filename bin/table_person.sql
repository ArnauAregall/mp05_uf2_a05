CREATE TABLE IF NOT EXISTS person 
(
        `id` INT(11) NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(50) NOT NULL,
        `surname` VARCHAR(50) NOT NULL,
        `age` INT(3) NOT NULL,
        `gender` VARCHAR(10) NOT NULL,
        `email` VARCHAR(100) NOT NULL,
        `address` VARCHAR(100) NOT NULL,
        `city` VARCHAR(100) NOT NULL,
        `postalcode` VARCHAR(10) NOT NULL,
        `phone` VARCHAR(10) NOT NULL,
        `creationdate` DATE,
        PRIMARY KEY (`id`)      
);

INSERT INTO person (name, surname, age, gender, email, address, city, postalcode, phone, creationdate) VALUES 
(
        'Arnau',
        'Aregall',
        20,
        'Male',
        'arnau@ar3soft.com',
        'Travessera de Dalt 14',
        '08024',
        'Barcelona',
        '699713828',
        CURDATE()
);

INSERT INTO person (name, surname, age, gender, email, address, city, postalcode, phone, creationdate) VALUES 
(
        'Eduard',
        'Coll',
        20,
        'Male',
        'eduard@ar3soft.com',
        'Travessera de Dalt 14',
        '08024',
        'Barcelona',
        '644326442',
        CURDATE()
);


